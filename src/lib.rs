use pyo3::prelude::*;

#[pyclass]
#[derive(Clone)]
struct Settings {
	inner: forceatlas2::Settings<f32>,
}

#[pymethods]
impl Settings {
	#[new]
	#[allow(clippy::too_many_arguments)]
	fn new(
		ka: f32,
		kg: f32,
		kr: f32,
		krprime: f32,
		lin_log: bool,
		prevent_overlapping: bool,
		speed: f32,
		strong_gravity: bool,
		theta: f32,
	) -> Self {
		Self {
			inner: forceatlas2::Settings {
				ka,
				kg,
				kr,
				lin_log,
				prevent_overlapping: prevent_overlapping.then(|| krprime),
				speed,
				strong_gravity,
				theta,
			},
		}
	}

	#[staticmethod]
	fn default() -> Self {
		Self {
			inner: Default::default(),
		}
	}
}

macro_rules! layout {
	($Layout:ident, $Settings:ident, $T:ident, $N:expr) => {
		#[pyclass]
		struct $Layout {
			inner: forceatlas2::Layout<$T, $N>,
		}

		#[pymethods]
		impl $Layout {
			#[staticmethod]
			fn from_abstract(
				settings: $Settings,
				nodes: Vec<(usize, $T, $T)>,
				edges: Vec<((usize, usize), $T)>,
			) -> Self {
				Self {
					inner: forceatlas2::Layout::<$T, $N>::from_abstract(
						settings.inner,
						nodes.into_iter().map(|(id, size, mass)| (id, forceatlas2::AbstractNode {size, mass})),
						edges,
					),
				}
			}

			fn iteration(&mut self) {
				self.inner.iteration();
			}

			fn get_point(&self, n: usize) -> Option<[$T; $N]> {
				self.inner.nodes.get(n).map(|node| node.pos.0)
			}

			#[getter]
			fn points(&self) -> Vec<[$T; $N]> {
				self.inner.nodes.iter().map(|node| node.pos.0).collect()
			}

			#[getter]
			fn get_settings(&self) -> $Settings {
				$Settings {
					inner: self.inner.get_settings().clone(),
				}
			}

			#[setter]
			fn set_settings(&mut self, settings: $Settings) {
				self.inner.set_settings(settings.inner);
			}
		}
	};
}

layout!(Layout2, Settings, f32, 2);
layout!(Layout3, Settings, f32, 3);

#[pymodule]
fn libfa2rs(m: &Bound<'_, PyModule>) -> PyResult<()> {
	m.add_class::<Settings>()?;
	m.add_class::<Layout2>()?;
	m.add_class::<Layout3>()?;

	Ok(())
}
