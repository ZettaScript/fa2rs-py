from fa2rs.libfa2rs import *

__all__ = ["__title__", "__summary__", "__uri__", "__version__", "__author__", "__email__", "__license__", "__copyright__"]

__author__ = "Pascal Engélibert <tuxmain@zettascript.org>"
__copyright__ = "CopyLeft 2020-2024 {0}".format(__author__)
__license__ = "AGPL 3.0"
__summary__ = "Bindings to full Rust ForceAtlas2 implementation"
__title__ = "ForceAtlas2-rs"
__uri__ = "https://framagit.org/ZettaScript/fa2rs-py"
__version__ = "0.7.0"
