#!/usr/bin/env python3

import random, time
import fa2rs

if __name__ == "__main__":
	n_nodes = 5000
	n_edges = 50000
	n_iter = 1000
	
	edges = []
	for i in range(n_edges):
		node_1 = random.randint(0, n_nodes-1)
		node_2 = random.randint(0, n_nodes-1)
		if node_2 == node_1:
			node_2 = (node_2+1) % n_nodes
		edges.append(((node_1, node_2), 1.0))
	
	nodes = [(i, 1.0, 1.0) for i in range(n_nodes)]

	# ---- fa2rs
	time_a = time.time()
	layout = fa2rs.Layout2.from_abstract(fa2rs.Settings.default(), nodes, edges)
	for i in range(n_iter):
		layout.iteration()
	time_b = time.time()
	print("fa2rs:       {}s".format(time_b - time_a))
