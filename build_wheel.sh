cargo build --release

mkdir -p target/wheel/src/fa2rs

cp py/setup.py target/wheel/
cp py/__init__.py target/wheel/src/fa2rs/
cp py/__test__.py target/wheel/src/fa2rs/
cp LICENSE target/wheel/
cp README.md target/wheel/
cp target/release/libforceatlas2_py.so target/wheel/src/fa2rs/libfa2rs.so

cd target/wheel

python3 setup.py sdist bdist_wheel
