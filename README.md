# ForceAtlas2 Python bindings from Rust

Python 3 bindings for Rust [forceatlas2](https://framagit.org/ZettaScript/forceatlas2-rs) crate: fast force-based graph spacialization layout, with optional multithread and SIMD.

![Example graph spacialized with ForceAtlas2](https://txmn.tk/img/wot-fa2rs.png)

Note: Cross-compiling with PyO3 is hard, so the distributed binary only works for GNU/Linux with latest versions of glibc (typically not Debian). Any help with cross-compilation or static glibc linking is welcome. :)

# Example

```
import fa2rs

edges = [(0,1), (1,2), (2,0), (2,3)]
layout = fa2rs.Layout.from_graph(edges, fa2rs.Settings(ka=0.1, kg=0.1, kr=0.1), nb_nodes=4)

for i in range(100):
    layout.iteration()

print(layout.points)
```

You may need to tweak the settings to fit your data. The most important parameter is the ratio between `ka` and `kr`. If the ratio has too low or high values, the graph will collapse or explode (resulting in zero or infinite values).

* `ka`: attraction coefficient (all neighbor nodes attract each other)
* `kg`: gravity coefficient (all nodes are attracted to the origin) (can be set to `0`)
* `kr`: repulsion coefficient (all nodes repulse each other)
* `chunk_size`: decrease to use more threads, increase to use fewer threads. Tune it to minimize both thread management time and unparallelized time. Set to `None` to disable parallelization.

# Build & install

Build:

    sh build_wheel.sh

Install:

    pip3 install target/wheel/dist/fa2rs-0.2.1-py3-none-any.whl --upgrade

More information about optimizations on [the Rust repository](https://framagit.org/ZettaScript/forceatlas2-rs).

`Layout` and `Settings` are aliases for `Layout32` and `Settings32`. The `32` and `64` versions refer to the size of the floats. 64-bit floats are more precise, but when SIMD is available, the layout is approximately twice slower than with 32-bit floats.

SIMD is only available on `x86` and `x86_64` with `avx2` feature. (check with `grep avx2 /proc/cpuinfo`)

## License

GNU AGPL v3, CopyLeft 2020-2024 Pascal Engélibert [(why copyleft?)](https://txmn.tk/blog/why-copyleft/)

The ForceAtlas2 paper was released under CC BY, Copyright 2014 Jacomy et al.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/.
